<?php

namespace LargeFileDl;

class Downloader {
    private $url;
    private $path;
    private $bufferSize;
    private $fpFrom;
    private $fpTo;
    private $isComplete;
    private $currentSize;
    private $allSize;
    private $options;

    function __construct() {
        $this->bufferSize = 1024 * 1024;
        $this->isComplete = false;
        $this->options = [];
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setSaveFilePath($path) {
        $this->path = $path;
    }

    function setOptions($options) {
        $this->options = $options;
    }

    function download() {
        if (!$this->fpFrom) {
            $context = stream_context_create($this->options);
            var_dump($context);
            $this->fpFrom = fopen($this->url, 'rb', false, $context);
            $this->fpTo = fopen($this->path, 'wb');
            $this->currentSize = 0;
            var_dump($http_response_header);
            preg_match('/HTTP\/1\.[0|1|x] ([0-9]{3})/', $http_response_header[0], $matches);
            $status_code = $matches[1];
            if ($status_code != 200) {
                throw new \Exception($http_response_header[0]);
            }
            preg_match('/(\d+)$/', $http_response_header[6], $m);
            $this->allSize = intval($m[1]);
        }
        $size = stream_copy_to_stream($this->fpFrom, $this->fpTo, $this->bufferSize);
        $this->currentSize += $size;
        if (feof($this->fpFrom)) {
            fclose($this->fpFrom);
            fclose($this->fpTo);
            $this->isComplete = true;
        }
    }

    function isCompleted() {
        return $this->isComplete;
    }

    function getCurrentSize() {
        return $this->currentSize;
    }

    function getAllSize() {
        return $this->allSize;
    }

}
